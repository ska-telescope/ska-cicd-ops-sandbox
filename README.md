# SKA Operations Sandbox

> This repository and its jobs are only accessible for the project maintainers instead of all SKAO developers

Operations Sandbox environment is created and deployed using the ska-mid chart in CAR in 
SDH&P Cluster with access control for deployment (retrigger of the job) using GitLab's pipeline using a separate repository.

## How to use it
This repository is configured only to manage this deployment from the main pipeline.
Go to the [list of pipelines](https://gitlab.com/ska-telescope/ska-cicd-ops-sandbox/-/pipelines),
and find the latest pipeline from the main branch.

![List of pipelines](docs/img/wQCNw1cOe3.png "List of pipelines")

We can remove all applications and services (clean button) and 
deploy them (or update if currently running) by clicking on the respective play button.

![Deployment operations](docs/img/ivmuZqwk3m.png "Deployment operations")

### Schedule redeployment

Every Sunday at 20 UTC, we have a scheduled pipeline that cleans and deploys the latest release on the main branch.
To create, edit or delete cron jobs, go to [CI/CD schedule jobs](https://gitlab.com/ska-telescope/ska-cicd-ops-sandbox/-/pipeline_schedules) 
page on Gitlab.

![Cron Job Page](docs/img/7jUbKisSEa.png "Cron Job Page")

## How to make changes

### Environment variables

On the `Makefile` file we have these variables available:
- **KUBE_NAMESPACE** (default: ops-sandbox)
    - Kubernetes deployment namespace;
- **HELM_RELEASE** (default: ska-ops-sandbox)
    - Name of the release;
- **K8S_CHART** (default: ops-sandbox)
    - Helm chart directory name;

### Helm values

On the `charts/ops-sandbox/values.yaml` file, we have all the available deployment variables.
Here, we can upgrade versions, change names, disable features, etc.


### Deploy changes

To deploy a new version, we need to update the main branch, but this is only allowed to project maintainers.
Other developers need to commit the changes, push them to a new branch and then wait for the maintainers' approval:

    # Create and move local git to branch
    git checkout -b <new-branch-name>

    # Add changed files (can use "." wildcard to select all files)
    git add <list-of-files-or-directories>

    # Commit message
    git commit -m "message"

    # Push changes to remote repository
    git push origin <new-branch_name>

After pushing all the changes, the [repository main page](https://gitlab.com/ska-telescope/ska-cicd-ops-sandbox) on Gitlab
adds an header section: 

!["Create New Merge Request](docs/img/zzAr5JiBhz.png "Create New Merge Request")

Click on the blue button above to create a merge request.