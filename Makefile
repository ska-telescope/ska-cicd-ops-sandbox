# Kubernetes namescope
KUBE_NAMESPACE=ops-sandbox

# Name of the helm release
HELM_RELEASE=ska-ops-sandbox

# Helm chart directory name
K8S_CHART=ops-sandbox

# include makefile targets for Kubernetes management
-include .make/k8s.mk

# include makefile targets for Kubernetes management
-include .make/helm.mk

## The following should be standard includes
# include core makefile targets for release management
-include .make/base.mk

# include your own private variables for custom deployment configuration
-include PrivateRules.mak

